
assembly_source_files :=  $(wildcard src/arch/risc-v/*.s)
assembly_object_files := $(patsubst src/%.asm, build/%.o, $(assembly_source_files))

c_source_files :=  $(wildcard src/*.c) $(wildcard src/drivers/uart/*.c) 

c_object_files := $(patsubst src/%.c, \
	build/%.o, $(c_source_files))

	
c_include = 'include'

CROSS_COMPILE :=riscv64-unknown-elf-
CC := $(CROSS_COMPILE)gcc
OBJCOPY := $(CROSS_COMPILE)objcopy
K210_BIN := os.bin
temp := temp
SBI := tools/rustsbi-k210
UART 		?= /dev/ttyUSB0
KFLASH 		:= tools/kflash.py
SU := sudo
            
CFLAGS:=-mcmodel=medany -march=rv64imafdc -mabi=lp64d -fno-pic -Wall -O  -ffreestanding -fno-common -nostdlib -mno-relax -nostdlib  \
-I ${c_include}

.DEFAULT_GOAL := all
all: ${K210_BIN}

run:$(K210_BIN)
	${OBJCOPY} ${SBI} --strip-all -O binary ${K210_BIN}
	dd if=$(temp).bin of=${K210_BIN} bs=128k seek=1 
	$(SU) chmod 777 $(UART) 
	$(PYTHON) $(KFLASH) -p $(UART) -b 1500000 -t $(K210_BIN)
	
# start.o must be the first in dependency!
${K210_BIN}: ${c_object_files} ${assembly_object_files}
	${CC} ${CFLAGS} -T link.ld -o $(temp).elf $^
	${OBJCOPY} -O binary $(temp).elf --strip-all -O binary $(temp).bin
	${OBJCOPY} ${SBI} --strip-all -O binary ${K210_BIN}
	dd if=$(temp).bin of=${K210_BIN} bs=128k seek=1
	 


build/%.o :src/%.c
	@mkdir -p $(shell dirname $@)
	${CC} ${CFLAGS} -c -o $@ $<

build/%.o : src/%.S
	@mkdir -p $(shell dirname $@)
	${CC} ${CFLAGS} -c -o $@ $<

build/%.o : src/%.s
	@mkdir -p $(shell dirname $@)
	${CC} ${CFLAGS} -c -o $@ $<
runqemu: all
	@${QEMU} -M ? | grep virt >/dev/null || exit
	@echo "Press Ctrl-A and then X to exit QEMU"
	@echo "------------------------------------"
	@${QEMU} ${QFLAGS} -kernel os.elf

.PHONY : debug
debug: all
	@echo "Press Ctrl-C and then input 'quit' to exit GDB and QEMU"
	@echo "-------------------------------------------------------"
	@${QEMU} ${QFLAGS} -kernel os.elf -s -S &
	@${GDB} os.elf -q -x ../gdbinit

.PHONY : code
code: all
	@${OBJDUMP} -S os.elf | less


.PHONY : clean
clean:
	rm -rf *.o *.bin *.elf
	@rm -rfv build

